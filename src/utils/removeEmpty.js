module.exports = function removeEmpty(obj) {
	Object.keys(obj).forEach(key => {
		const val = obj[key];
		if (val === null || val === "" || (val.constructor === Object && Object.keys(val).length === 0)) {
			return delete obj[key];
		}

		if (val.constructor === Object) {
			return removeEmpty(val);
		}
	});
	return obj;
};
