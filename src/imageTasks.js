const exif = require('./tasks/exif');

/**
 * List the tasks to run once we have the image source
 * @type {Array}
 */
module.exports = [ exif ];
