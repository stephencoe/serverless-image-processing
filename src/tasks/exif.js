const ExifImage = require('exif').ExifImage;
const removeEmpty = require('../utils/removeEmpty');

/**
 * Clean the data ready to be saved
 */
const cleanData = (data) => {
	data = JSON.parse(JSON.stringify(data).replace(/\\u0000/g, ''));

	// delete buffers that I dont want to store
	delete data.exif.MakerNote
	delete data.exif.UserComment
	delete data.interoperability

	// remove empty objects and values
	return removeEmpty(data);
};

const run = (body) => {
	return new Promise((resolve,reject) => {
		try {
			new ExifImage(body, (error, exifData) => {
				if (error) {
					console.log(`Error: ${error.message}`);
					return reject(error);
				}
				exifData = cleanData(exifData);

				let created = null;
				let original = null;
				let modified = null;

				if('exif' in exifData) {
					created = 'CreateDate' in exifData.exif ? exifData.exif.CreateDate : null;
					original = 'DateTimeOriginal' in exifData.exif ? exifData.exif.DateTimeOriginal : null;
				}

				if('image' in exifData) {
					modified = 'ModifyDate' in exifData.image ? exifData.image.ModifyDate : null;
				}

				resolve({
					exifData: exifData,
					created,
					original,
					modified,
				});
			});
		} catch (error) {
			console.log(`Error: ${error.message}`);
			return reject(error);
		}
	});
};

exports.run = run;
