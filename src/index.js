const AWS = require('aws-sdk');
const s3 = new AWS.S3({apiVersion: '2006-03-01'});
const sqs = new AWS.SQS({region:'eu-west-1'});
const checksum = require('./utils/checksum');
const dynamoConfig = require('./config/dynamo');
const sqsConfig = require('./config/sqs');
const imageTasks = require('./imageTasks');

const handler = (event, context) => {
	console.log(JSON.stringify(event));
	const record = event.Records[0];
	const s3Params = {
	   Bucket: record.s3.bucket.name,
	   Key : record.s3.object.key
	};

	s3.getObject(s3Params)
		.promise()
		.then(response => {
 			const dynamoParams = Object.assign(dynamoConfig, {
				Item: {
					path: record.s3.object.key,
					imageHash: checksum(response.Body),
					timeStamp: record.eventTime
				}
			});

			const tasks = imageTasks.map(task => task.run(response.Body));
			//
			// wait for all the promises to resolve
			// save when its done
			//
			Promise.all(tasks).then(results => {
				results.forEach(result => {
					Object.keys(result)
						.forEach(key => dynamoParams.Item[key] = result[key]);
				});

				const sqsParams = Object.assign(sqsConfig, {
					MessageBody: JSON.stringify(dynamoParams),
				});

				sqs.sendMessage(sqsParams)
					.promise()
					.then(data => {
						console.log('SUCCESS: ' + JSON.stringify(data));
						context.succeed(data);
					}).catch(err => console.log(err));
			});
		 })
		.catch(err => console.log(err));
};

exports.handler = handler;
