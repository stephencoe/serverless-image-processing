# Serverless image processing

Extract exif imformation for images uploaded to S3 using Lambda.

## Flow
- Image arrives in an S3 bucket which is configured to trigger a lambda
- Lambda downloads the image from S3, extracts the data
- Lambda pushes the data to SQS for later processing

**As we do not need the data straight away we push the processed data into SQS for later processing.

## Getting started

- AWS environments are managed using [terraform](https://www.terraform.io/).
- Code is in node, this assumes you are running 6+

## Setup
Create the `infrastructure/terraform.tfvars` file to configure your functions

```
aws_access_key = ""
aws_secret_key = ""
aws_region = ""
bucket_key = ""
sqs_queue = ""
```

`sqs_queue` should be the name of the queue you wish to use. For example, 'IMAGE_PROCESSING'
`bucket_key` should be the name of the folder where your images arrive on S3

## Install
Run `(yarn|npm) install` depending on which you prefer to use

## Build
Run `(yarn|npm) run build` depending on which you prefer to use

## Deploy
Run `(yarn|npm) run deploy` depending on which you prefer to use

## Testing
Testing lambda

## Next
From here there are many things we can do such as resize images, trigger rekognition to perform object detection
