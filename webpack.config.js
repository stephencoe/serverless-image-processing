const webpack = require('webpack');
const path = require('path');

const config = {
	externals: ["aws-sdk"],
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /(node_modules)/,
			use: {
				loader: 'babel-loader',
				options: {
					presets: ['env']
				}
			}
		}]
	},
	output: {
		libraryTarget: "commonjs",
		path: path.join(__dirname, 'build'),
		filename: "[name].js"
	},
	target: "node",
	entry: "./src/index.js",
	plugins: []
};

if (1) {
	config.plugins.push(new webpack.optimize.UglifyJsPlugin())
}

module.exports = config;
