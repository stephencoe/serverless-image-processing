variable "sqs_queue" {}

resource "aws_sqs_queue" "process_images" {
  name = "${var.sqs_queue}"
}
