resource "aws_iam_role_policy" "process_images" {
	name = "process_images"
	role = "${aws_iam_role.pic_db_role.id}"
	policy = <<EOF
{
	"Version": "2012-10-17",
	"Statement": [
		{
			"Effect": "Allow",
			"Action": [
				"s3:Get*",
				"s3:List*",
				"logs:*",
				"sqs:*"
			],
			"Resource": "*"
		}
	]
}
EOF
}

resource "aws_lambda_permission" "allow_image_processing" {
	statement_id  = "AllowExecutionFromS3Bucket"
	action        = "lambda:InvokeFunction"
	function_name = "${aws_lambda_function.process_s3_image.arn}"
	principal     = "s3.amazonaws.com"
	source_arn    = "${aws_s3_bucket.bucket.arn}"
}

resource "aws_lambda_function" "process_s3_image" {
	runtime = "nodejs6.10"
	filename      = "../build/main.zip"
	function_name = "process_s3_image"
	source_code_hash = "${base64sha256(file("../build/main.zip"))}"
	role          = "${aws_iam_role.pic_db_role.arn}"
	handler       = "main.handler"

	environment {
		variables = {
			SQS_QUEUE = "${aws_sqs_queue.process_images.arn}"
		}
	}
}
