variable "bucket_key" {}

resource "aws_s3_bucket" "bucket" {
	bucket = "labs.madebybox.co.uk"
}

resource "aws_s3_bucket_notification" "s3_images" {
	bucket = "${aws_s3_bucket.bucket.id}"

	lambda_function {
		lambda_function_arn = "${aws_lambda_function.process_s3_image.arn}"
		events              = ["s3:ObjectCreated:*"]
		filter_prefix       = "${var.bucket_key}"
		filter_suffix       = ".jpg"
	}
}
