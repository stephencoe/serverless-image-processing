const fs = require('fs');
const foo = require('../src/index');

let event = JSON.parse(fs.readFileSync('./event.json'));

const contextMock = {
	succeed: () => console.log('succeed'),
	fail: ()=> console.log('fail'),
};

foo.handler(event, contextMock)
